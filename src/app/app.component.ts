import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <app-navbar></app-navbar>
    <app-body></app-body>
  `
})
export class AppComponent {
  title = 'app';
}
