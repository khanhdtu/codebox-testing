import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRouterModule } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap';
import { FormsModule } from '@angular/forms';
import { MomentModule } from 'angular2-moment';
import { CustomFormsModule } from 'ng2-validation';

import { AppComponent } from './app.component';
import { NavbarComponent } from './container/navbar/navbar.component';
import { BodyComponent } from './container/body/body.component';
import { HomeComponent } from './container/body/components/home/home.component';
import { CartComponent } from './container/body/components/cart/cart.component';

import { AppService } from './services/apis/app.service';
import { BsModalService } from 'ngx-bootstrap';
import { AppSubjectService } from './services/subjects/app.subject';

import { CountPipe } from './pipes/count.pipe';
import { WishComponent } from './container/body/components/wish/wish.component';
import { UserComponent } from './container/body/components/user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    BodyComponent,
    HomeComponent,
    CartComponent,
    WishComponent,
    UserComponent,
    CountPipe
  ],
  imports: [
    BrowserModule,
    AppRouterModule,
    HttpClientModule,
    ModalModule.forRoot(),
    FormsModule,
    MomentModule,
    CustomFormsModule
  ],
  providers: [
    AppService,
    BsModalService,
    AppSubjectService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
