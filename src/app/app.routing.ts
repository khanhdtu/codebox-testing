import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './container/body/components/home/home.component';
import { WishComponent } from './container/body/components/wish/wish.component';
import { UserComponent } from './container/body/components/user/user.component';

const routes: Routes = [
    {
        path: 'Home',
        component: HomeComponent
    },
    {
        path: 'Wish',
        component: WishComponent
    },
    {
        path: 'User',
        component: UserComponent
    },
    {
        path: '',
        redirectTo: 'Home',
        pathMatch : 'full'
    }
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRouterModule { }
