export class CONSTS {
    public static ROOT_URL = 'https://testing-3ab32.firebaseio.com/products.json';
    public static RESPONSE = {
        STATUS : {
            SUCCESS: 1,
            WARNING: 2,
            ERROR: 3
        },
        MESSAGES: {
            BuySuccess: 'Thank you for purchasing out product !',
            BuyError: 'Please try again !'
        }
    };
}
