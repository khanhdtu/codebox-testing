import { Component, OnInit, Input } from '@angular/core';
import { Product } from '../../../../models/product.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AppSubjectService } from '../../../../services/subjects/app.subject';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  @Input() product: Product;
  @Input() ordered: Product[];
  @Input() modalRef: any;
  public quantity: number;
  public priceTotal: number;
  constructor(
      private modalService: BsModalService,
      private appSubjectService: AppSubjectService
    ) {
      this.quantity = 1;
  }

  onChange() {
    this.priceTotal = this.product.price * this.quantity;
  }

  onAdd() {
    const found = this.ordered.find(e => {
        return e.id === this.product.id;
    });
    this.product.order = {
        added : true,
        quantity: this.quantity
    };
    if (found) {
        const index = this.ordered.findIndex((item) => {
            return item.id === this.product.id;
        });
        this.ordered[index] = this.product;
    } else {
        this.ordered.push(this.product);
    }
    localStorage.setItem('Cart', JSON.stringify(this.ordered));
    this.appSubjectService.set(this.ordered);
    this.modalRef.hide();
  }

  ngOnInit() {
    this.priceTotal = this.product.price * this.quantity;
  }
}
