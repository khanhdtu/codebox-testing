import { Component, OnInit } from '@angular/core';
import { AppService } from '../../../../services/apis/app.service';
import { Product } from '../../../../models/product.model';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { AppSubjectService } from '../../../../services/subjects/app.subject';
import { Router } from '@angular/router';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public items: Product[];
  public ordered: Product[];
  public wishList: Product[];
  public modalRef: BsModalRef;
  constructor(
    private appService: AppService,
    private appSubjectService: AppSubjectService,
    private modalService: BsModalService,
    private activateRoute: Router
  ) {
    const cart = localStorage.getItem('Cart');
    const wishList = localStorage.getItem('WishList');
    if (cart) {
      this.ordered = JSON.parse(cart);
    } else {
      this.ordered = [];
    }
    if (wishList) {
      this.wishList = JSON.parse(wishList);
    } else {
      this.wishList = [];
    }
  }

  onOpenCart(template: any, product: Product) {
    this.modalRef = this.modalService.show(template);
  }

  onWish(product: Product) {
    product.order.timestamp = new Date().getTime();
    const index = this.wishList.findIndex(item => {
      return item.id === product.id;
    });
    if (index === -1) {
      this.wishList.push(product);
    }
    console.log(this.wishList)
    localStorage.setItem('WishList', JSON.stringify(this.wishList));
    this.activateRoute.navigate(['/Wish']);
  }

  ngOnInit() {
    this.appService.getProducts()
    .toPromise()
    .then(items => {
      this.items = items;
    })
    .catch(error => {
      console.error(error);
    });
    this.appSubjectService.set(this.ordered);
  }
}
