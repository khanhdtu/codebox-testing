import { Component, OnInit } from '@angular/core';
import { User } from '../../../../models/user.model';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  public user: User;
  constructor() {
    this.user = {
      address: '',
      email: '',
      firstName: '',
      lastName: '',
      password: '',
      phone: '',
      notes: ''
    };
  }

  ngOnInit() {
  }

}
