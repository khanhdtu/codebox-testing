import { Component, OnInit, Input } from '@angular/core';
import { AppSubjectService } from '../../../../services/subjects/app.subject';
import { Product } from '../../../../models/product.model';

@Component({
  selector: 'app-wish',
  templateUrl: './wish.component.html',
  styleUrls: ['./wish.component.scss']
})
export class WishComponent implements OnInit {
    public wishList: Product[];
    constructor(
      private appSubjectService: AppSubjectService
    ) {
        this.wishList = JSON.parse(localStorage.getItem('WishList'));
    }

    onRemove(index: number) {
      this.wishList.splice(index, 1);
      localStorage.setItem('WishList', JSON.stringify(this.wishList));
    }

    ngOnInit() {
    }
}
