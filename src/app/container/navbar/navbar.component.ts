import { Component, OnInit, AfterContentInit, AfterViewInit } from '@angular/core';
import { AppSubjectService } from '../../services/subjects/app.subject';
import { Product } from '../../models/product.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public cart: Product[];

  constructor(private appSubjectService: AppSubjectService) {
    this.cart = JSON.parse(localStorage.getItem('Cart'));
  }

  ngOnInit() {
    this.appSubjectService.cartObs
    .asObservable()
    .subscribe(cart => {
      this.cart = cart;
    });
  }
}
