export interface Product {
    id: string;
    name: string;
    price: number;
    thumb: string;
    description?: string;
    order?: {
        added?: boolean,
        wish?: boolean,
        quantity?: number,
        timestamp?: number
    };
}
