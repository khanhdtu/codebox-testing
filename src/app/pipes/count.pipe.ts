import { Pipe, PipeTransform } from '@angular/core';
import { Product } from '../models/product.model';

@Pipe({
  name: 'count',
  pure: false
})
export class CountPipe implements PipeTransform {
  transform(items: Product[], args?: any): any {
    let price = 0;
    if (items) {
      items.forEach(item => {
        if (item.order.added) {
            price += item.order.quantity * item.price;
        }
      });
      return price;
    }
  }
}
