import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CONSTS } from '../../constants/app.constant';
import { Product } from '../../models/product.model';
@Injectable()
export class AppService {
    public url: string;
    constructor( private http: HttpClient) {
        this.url = CONSTS.ROOT_URL;
    }

    getProducts() {
        return this.http.get<Product[]>(this.url);
    }
}
