import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from '../../models/product.model';
@Injectable()
export class AppSubjectService {
    public cartObs = new Subject<Product[]>();
    constructor() {}

    set(cart: Product[]) {
        this.cartObs.next(cart);
    }
}
